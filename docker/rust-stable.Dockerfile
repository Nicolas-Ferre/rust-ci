ARG RUST_STABLE_VERSION
FROM rust:${RUST_STABLE_VERSION}-slim

ENV KCOV_VERSION 38
ENV TMP_DEPENDENCIES cmake wget build-essential python3

# Install system packages
RUN apt-get update
RUN apt-get install -y $TMP_DEPENDENCIES zlib1g-dev libelf-dev libcurl4-openssl-dev libdw-dev libbfd-dev libiberty-dev # kcov dependencies
RUN apt-get install -y pkg-config libssl-dev # other dependencies

# Install kcov
RUN wget https://github.com/SimonKagstrom/kcov/archive/$KCOV_VERSION.tar.gz
RUN tar xzf $KCOV_VERSION.tar.gz
WORKDIR kcov-$KCOV_VERSION
RUN mkdir build
WORKDIR build
RUN cmake ..
RUN make
RUN make install
WORKDIR ../..

# Install Rust components
RUN rustup component add clippy rustfmt
RUN cargo install cargo-audit cargo-kcov cargo-outdated sccache

# Uninstall unused dependencies
RUN rm -rf $KCOV_VERSION.tar.gz kcov-$KCOV_VERSION
RUN apt-get autoremove -y $TMP_DEPENDENCIES
