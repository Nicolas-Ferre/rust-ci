ARG RUST_NIGHTLY_VERSION
ARG MUTAGEN_COMMIT
FROM instrumentisto/rust:nightly-stretch-slim-${RUST_NIGHTLY_VERSION}

# Install system packages
RUN apt-get update
RUN apt-get install -y git

# Install mutagen
RUN git clone https://github.com/llogiq/mutagen
RUN cd mutagen && git checkout $MUTAGEN_COMMIT
RUN cargo install --path ./mutagen/mutagen-runner
RUN rm -rf mutagen

# Install other tools
RUN apt-get install -y pkg-config libssl-dev
RUN cargo install sccache
