#!/bin/bash
set -eu

docker login registry.gitlab.com
docker build -t registry.gitlab.com/"$GITLAB_PROJECT_PATH"/ci-alpine . -f ./docker/alpine.Dockerfile
docker build --build-arg RUST_STABLE_VERSION="$RUST_STABLE_VERSION" -t registry.gitlab.com/"$GITLAB_PROJECT_PATH"/ci-rust-stable . -f ./docker/rust-stable.Dockerfile
docker build --build-arg RUST_NIGHTLY_VERSION="$RUST_NIGHTLY_VERSION" --build-arg MUTAGEN_COMMIT="$MUTAGEN_COMMIT" -t registry.gitlab.com/"$GITLAB_PROJECT_PATH"/ci-rust-nightly . -f ./docker/rust-nightly.Dockerfile
docker push registry.gitlab.com/"$GITLAB_PROJECT_PATH"/ci-alpine
docker push registry.gitlab.com/"$GITLAB_PROJECT_PATH"/ci-rust-stable
docker push registry.gitlab.com/"$GITLAB_PROJECT_PATH"/ci-rust-nightly
