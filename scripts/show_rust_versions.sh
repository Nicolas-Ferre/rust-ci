#!/bin/bash
set -eu

rustup --version
rustc --version
cargo --version
