# Rust CI

GitLab CI configuration for Rust projects

## Setup a GitLab repository

Add this repository as submodule in the `./ci` folder:
```shell script
git submodule add -b master https://gitlab.com/Nicolas-Ferre/rust-ci.git ./ci
cd ci
git checkout v0.1.0
```

Create an `audit.toml` file in the root directory with at least the following content:
```toml
[advisories]
ignore = []
```

Create a `env.sh` file in the root directory with the following variables (you have to modify the values if necessary):
```shell script
export GITLAB_PROJECT_PATH=project/path # where your repository URL is https://gitlab.com/project/path, must be lowercase
export CRATE_PATHS="." # "crate1;crate2" if your project is a workspace with two crates crate1 and crate2 (quotes are important)
export RUST_STABLE_VERSION=1.47
export RUST_NIGHTLY_VERSION=2020-11-06
export MUTAGEN_COMMIT=f8249256c40769c916b5b00bd284f204d5540588
export SCCACHE_CACHE_SIZE=1G
export KCOV_EXCLUDED_LINES="struct ,#[derive(, }, );,match "
export KCOV_EXCLUDED_REGION=coverage=off:coverage=on
export KCOV_THRESHOLD=100.0
export MUTAGEN_THRESHOLD=100.0
```

Create a `.gitlab-ci.yml` in the root directory with the following content:
```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GITLAB_PROJECT_PATH: project/path # where your repository URL is https://gitlab.com/project/path, must be lowercase

include:
  - remote: https://gitlab.com/Nicolas-Ferre/rust-ci/raw/v0.1.0/jobs.yml
```

## Push job images

In order to make the pipeline work, it is required to push the Docker images of the jobs in the container registry of your GitLab repository.

To do that, you can run the following variables from the root directory of your local repository:
```shell script
source env.sh
cd ci
sh deploy_images.sh
```

## Publish on crates.io

In order to be able to publish your crates on crates.io using the pipeline, you have to provide the environment variable `CRATES_IO_TOKEN`.
This will add to the pipeline a manual job to publish your crates.
